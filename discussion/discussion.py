# Input
# Input() allows us to gather data from the user input, returns "string" data type
# "\n" stands for the line break

# username = input("Please enter your name: ")
# print(f"Hi {username}! Welcome to Python Short Course")

# num1 = int(input("Enter num1: "))
# num2 = int(input("Enter num2: "))

# print(f"The sum of num1 and num2 is {num1+num2}")

# if-else statements
# if-else statements are used to choose between two or more code blocks depending on the condition

# Declare a variable to use for the condition statement

# test_num = 75

# if test_num >= 60:
# 	print("Test Passed")
# else:
# 	print("Test failed")

# Else-if chains

# test_num2 = int(input("Enter test_num2: "))

# if test_num2 > 0:
# 	print("Positive")
# elif test_num2 == 0:
# 	print("Zero")
# else:
# 	print("Negative")

# Mini-Exercise:
# Create an if-else statement that determines if a number is divisible by 3, 5 or both
# If the number is divisible by 3, print("The number is divisible by 3")
# If the number is divisible by 5, print("The number is divisible by 5")
# If the number is divisible by 5 and 3, print("The number is divisible by both 3 and 5")
# if the number is not divisible by any, print("The number is not divisible by 3 not 5")

# SOLUTION: 

# for x in range(3):
# 	val = int(input("Enter a value: "));
# 	if val % 15 == 0:
# 		print("The number is divisible by both 3 and 5")
# 	elif val % 3 == 0:
# 		print("The number is divisible by 3")
# 	elif val % 5 == 0:
# 		print("The number is divisible by 5")
# 	else:
# 		print("The number is not divisible by both 3 nor 5")

# Loops
# Python has loops that can repeat blocks of code
# While Loops are used to executing a set of statement as long as the condition is true

# i = 1
# while i <= 5:
# 	print(i)
# 	i += 1

# For Loops are used for iterating over a sequence

# fruits = ["apple", "banana", "cherry"]
# for x in fruits:
# 	print(x)

# range() method
# To use the for loop to iterate through values, the range method can be used
# Syntax:
# range(stop)

# The range() - defaults to 0 as a starting value. 0 - 5

# for x in range(6):
# 	print(x)

# for x in range(6, 10):
# 	print(x)

# for x in range(6, 10, 2):
# 	print(x)

# Break Statements
# The break statement is used to stop the loop

# j = 1
# while j < 6:
# 	print(j)
# 	if j == 3:
# 		break
# 	j += 1

# Continue Statement
# Continue Statement it returns the control to the beginning of the while loop and continue with the next iteration

k = 1
while k < 6:
	k += 1
	if k == 3:
		continue
	print(k)