leap = int(input("Please input a year: "))

if leap <= 0:
	print(f"{leap} is zero or negative!")
else :
	if leap % 100 == 0:
		print(f"{leap} is not leap year")
	elif leap % 400 == 0:
		print(f"{leap} is a leap year")
	elif leap % 4 == 0:
		print(f"{leap} is a leap year")
	else :
		print(f"{leap} is not leap year")

row = int(input("Enter number of rows: "))
col = int(input("Enter number of columns: "))

for x in range(row):
	for y in range(col):
		print("*", end = '')
	print("")